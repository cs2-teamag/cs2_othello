import java.util.BitSet;
import java.util.Vector;
import java.util.Iterator;

/**
 * Implements a standard 8x8 Othello board.
 * <strong>TODO:</strong> Streamline.
 *
 * @author Brandon Moore
 **/
public class OthelloBoard
{
   //locations that have a black piece
   BitSet black = new BitSet(64);
   //locations that have any piece.
   BitSet taken = new BitSet(64);
   // weight on corner pieces
   public static final int CORNER_WEIGHT = 50;
   // weight on the number of available moves we have - moves the opponent has
   public static final int MOVES_WEIGHT = 2;
   public static final int DIAGONAL_WEIGHT = -10;
   public static final int EDGE_WEIGHT = 5;
   // weight on frontier pieces
   public static final int FRONTIER_WEIGHT = 2;

    public static int boolToInt(boolean b) {
        return b ? 1 : 0;
    }

   public boolean occupied(int x, int y) {
      return taken.get(x + 8*y);
   }

   public boolean get(OthelloSide side, int x, int y) {
      return occupied(x, y) && (black.get(x + 8*y) == (side == OthelloSide.BLACK));
   }

   public void set(OthelloSide side, int x, int y) {
      taken.set(x + 8*y);
      black.set(x + 8*y, side == OthelloSide.BLACK);
   }

    public boolean equals(Object obj) {
        if (obj instanceof OthelloBoard) {
            OthelloBoard other = (OthelloBoard) obj;
            return (black.equals(other.black) && taken.equals(other.taken));
        }
        return false;
    }
   /**
    * Returns a copy of this board.
    **/
   public OthelloBoard copy() {
      OthelloBoard newBoard = new OthelloBoard();
      newBoard.black = (BitSet)this.black.clone();
      newBoard.taken = (BitSet)this.taken.clone();

      return newBoard;
   }

   /**
    * Make a standard 8x8 othello board.
    * Initialize to the standard setup.
    **/
   public OthelloBoard() {
      //Standard setup with 4 pieces in the center.
      taken.set(3 + 8 * 3);
      taken.set(3 + 8 * 4);
      taken.set(4 + 8 * 3);
      taken.set(4 + 8 * 4);
      black.set(4 + 8 * 3);
      black.set(3 + 8 * 4);
   }

   /**
    * Tests if the game is finished. The game is finished if neither
    * side has a legal move.
    * @return true if there are no legal moves.
    **/
   public boolean isDone() {
      return !(hasMoves(OthelloSide.BLACK) || hasMoves(OthelloSide.WHITE));
   }

   /**
    * Tests for legal moves.
    * @param side Othelloside to check for valid moves.
    * @return true if there are legal moves.
    **/
   public boolean hasMoves(OthelloSide side) {
      for (int i = 0; i < 8; i++) {
         for (int j = 0; j < 8; j++) {
            if (checkMove(new Move(i, j), side)) {
               return true;
            }
         }
      }
      return false;
   } 

   // Returns a list of possible moves for the specified side
    public Vector<Move> getMoves(OthelloSide side) {
        Vector<Move> movesList = new Vector<Move>();
        Move move;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                move = new Move(i, j);
                if (checkMove(move, side)) {
                   movesList.add(move);
                }
            }
        }
        return movesList;
    }

    // get the number of possible moves the opponent can make after we make ours
    public int getNextMoveScore(OthelloSide side) {
        Vector<Move> opponentMoves = getMoves(side.opposite());
        return -opponentMoves.size();
    }

    // get the number of the opponent's frontier pieces (good) - our frontier pieces (bad)
    public int getFrontierScore(OthelloSide side) {
        int score = 0;
        boolean frontier = false;
        for (int x = 1; x < 7; x++) {
            for (int y = 1; y < 7; y++) {
                // if the piece is ours, check if it's a frontier
                if (get(side, x, y)) {
                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            if (dx == 0 && dy == 0)
                                continue;
                            // if any of the eight neighboring squares is unoccupied, it's a frontier
                            else if (!occupied(x, y)) {
                                frontier = true;
                            }
                        }
                    }
                    if (frontier) 
                        score--;
                }
                // do the same thing for the opponent's pieces
                else if (get(side.opposite(), x, y)) {
                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            if (dx == 0 && dy == 0)
                                continue;
                            else if (!occupied(x, y)) {
                                frontier = true;
                            }
                        }
                    }
                    if (frontier)
                        score++;
                }
                // reset the frontier variable
                frontier = false;
            }
        }
        return score;
    }

    // Returns the score for a particular board state
    public int getScore(OthelloSide side) {
        int score = 0;
     //   move(moveToMake, side);
        // add our pieces, subtract opponent's pieces
        if (side == OthelloSide.BLACK) {
            score += countBlack();
            score -= countWhite();
        }
        else {
            score += countWhite();
            score -= countBlack();
        }
        // add weight for the corners (or subtract if they're the opponent's)
        if (get(side, 0, 0) || get(side, 0, 7) || get(side, 7, 0) 
            || get(side, 7, 7)) {
            score += CORNER_WEIGHT * (boolToInt(get(side, 0, 0)) + boolToInt(get(side, 0, 7)) 
                + boolToInt(get(side, 7, 0)) + boolToInt(get(side, 7, 7)));
        }
        if (get(side.opposite(), 0, 0) || get(side.opposite(), 0, 7) || get(side.opposite(), 7, 0) 
            || get(side.opposite(), 7, 7)) {
            score -= CORNER_WEIGHT * (boolToInt(get(side.opposite(), 0, 0)) + boolToInt(get(side.opposite(), 0, 7)) 
                + boolToInt(get(side.opposite(), 7, 0)) + boolToInt(get(side.opposite(), 7, 7)));
        }
        // really negative weight for spots diagonal to unoccupied corners
        if (get(side, 1, 1) && !get(side.opposite(), 0, 0)) {
            score += DIAGONAL_WEIGHT;
        }
        if (get(side, 1, 6) && !get(side.opposite(), 0, 7)) {
            score += DIAGONAL_WEIGHT;
        } 
        if (get(side, 6, 1) && !get(side.opposite(), 7, 0)) {
            score += DIAGONAL_WEIGHT;
        } 
        if (get(side, 6, 6) && !get(side.opposite(), 7, 7)) {
            score += DIAGONAL_WEIGHT;
        }
        // add weight for edges
        for (int x = 0; x < 8; x += 7) {
            for (int y = 2; y < 6; y++) {
                score += EDGE_WEIGHT * boolToInt(get(side, x, y));
            }
        }
        for (int y = 0; y < 8; y += 7) {
            for (int x = 2; x < 6; x++) {
                score += EDGE_WEIGHT * boolToInt(get(side, x, y));
            }
        }
        score += MOVES_WEIGHT * getNextMoveScore(side);
        score += FRONTIER_WEIGHT * getFrontierScore(side);
        return score;
    }

    boolean onBoard(int x, int y) {
        return(0 <= x && x < 8 && 0 <= y && y < 8);
    }

   /**
    * Tests if a move is legal.
    * @param m The move being made
    * @param turn The player making the move.
    **/
   //might be able to do clever stuff with masks and next clear bit
   //and the like.
   public boolean checkMove(Move m, OthelloSide turn) {
      if(m == null)
         //passing is only legal if you have no moves
         return !hasMoves(turn);

      // Make sure the square hasn't already been taken.
      if(occupied(m.getX(), m.getY()))
         return false;

      OthelloSide other = turn.opposite();
      int X = m.getX();
      int Y = m.getY();
      for (int dx = -1; dx <= 1; dx++) {
         for (int dy = -1; dy <= 1; dy++) {
            //for each direction
            if (dy == 0 && dx == 0)
               continue;

            //is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x,y) && get(other,x,y)) {
               do {
                  x += dx;
                  y += dy;
               } while (onBoard(x,y) && get(other,x,y));
               if (onBoard(x,y) && get(turn,x,y)) {
                  return true;
               }
            }
         }
      }
      return false;
   }

   /**
    * Modifies the board to reflect the specified move.
    * @param m The move being made
    * @param turn The player making the move
    **/
   public void move(Move m, OthelloSide turn) {
      // null means pass.
      if (m == null)
        return;

      if (!checkMove(m, turn)) {
            throw new InternalError("Invalid Move " + m);
      }

      OthelloSide other = turn.opposite();
      for (int dx = -1; dx <= 1; dx++) {
         for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) {
               continue;
            }
            int x = m.getX();
            int y = m.getY();
            do {
               x += dx;
               y += dy;
            } while (onBoard(x,y) && get(other,x,y));
            if (onBoard(x,y) && get(turn,x,y)) {
               x = m.getX();
               y = m.getY();
               x += dx;
               y += dy;
               while (onBoard(x,y) && get(other,x,y)) {
                  set(turn,x,y);
                  x += dx;
                  y += dy;
               }
            }
         }
      }
      set(turn,m.getX(),m.getY());
   }

   /**
    * Current count of black stones.
    * @return The number of black stones on the board.
    **/
   public int countBlack() {
      return black.cardinality();
   }

   /**
    * Current count of white stones.
    * @return The number of white stones on the board.
    **/
   public int countWhite() {
      BitSet result = (BitSet)taken.clone();
      result.andNot(black);
      return result.cardinality();
   }
}
