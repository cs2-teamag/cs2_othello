import java.util.*;

public class DecisionTree {
	// parent node of the tree
	private Node root;

    // maximizer
    protected static OthelloSide maximizer;

	public DecisionTree(OthelloBoard board, OthelloSide side) {
        maximizer = side.opposite();
        root = new Node(null, side, board);
	}

    // Finds the best move out of the root node's children (because the root node represents 
    // the current board, and we need to choose a move to make from its children)
    public Move findBestMove(int depth) {
        search(root, depth);
        // Initialize the node and iterator
        Node bestNode = root.getChildren().getFirst();
        Iterator itr = root.getChildren().iterator();
        Node currentNode = bestNode;
        while (itr.hasNext()) {
            currentNode = (Node) itr.next();
            // Determine the best node based on the one with the highest beta
            if (currentNode.getBeta() > bestNode.getBeta()) {
                bestNode = currentNode;
            }
        }
        return bestNode.getMove();
    }

    // Generates the tree down to the specified depth as well as the 
    // average scores for all of the nodes
	public void search(Node startingNode, int depth) {
        if (depth < 0) {
            return;
        }
        startingNode.findChildren();
		Iterator itr = startingNode.getChildren().iterator();
        while (itr.hasNext()) {
            // Search all the node's children
            Node child = (Node) itr.next();
            search(child, depth - 1);
            if (depth == 0) {
                child.setAlpha(child.getScore());
                child.setBeta(child.getScore());
            }
            // the side of the starting node is the opposite of the one making the move
            if (startingNode.getSide() == maximizer) {
                startingNode.setBeta(Math.min(startingNode.getBeta(), child.getAlpha()));
            }
            else {
                startingNode.setAlpha(Math.max(startingNode.getAlpha(), child.getBeta()));
            }
            if (startingNode.getAlpha() > startingNode.getBeta()) {
                startingNode.getParent().getChildren().remove(startingNode);
                return;
            }            
        }
	}

	public static class Node {
        // The board state of the node
        private OthelloBoard board;
        // The move associated with this board state, i.e. the move that 
        // led to this node
		private Move move;
        // The side that's making the next move
		private OthelloSide side;
        // Score for this node's board state
        private double score;
        // alpha
		private double alpha;
        // beta
        private double beta;
        // Parent node
		private Node parent;
        // Children of the node
		private LinkedList<Node> children;

		public Node(Move move, OthelloSide side, OthelloBoard board) {
			this.board = board;
            this.move = move;
            this.side = side;
            if (side == maximizer) {
                this.score = (float) board.getScore(side);
            }
            else {
                this.score = (float) board.getScore(maximizer);
            }
            this.alpha = Double.NEGATIVE_INFINITY;
            this.beta = Double.POSITIVE_INFINITY;
            this.parent = null;
			this.children = new LinkedList<Node>();
		}

		public void addChild(Node node) {
			node.parent = parent;
			children.add(node);
		}

        // Creates the node's list of children and sets the node's alpha and beta
        public void findChildren() {
            OthelloBoard newBoard = board.copy();
            // Get the possible moves for this board state
            Vector<Move> moves = newBoard.getMoves(side.opposite());
            Move childMove;
            Iterator itr = moves.iterator();
            while (itr.hasNext()) {
                childMove = (Move) itr.next();
                newBoard.move(childMove, side.opposite());
                // Make a new node for every possible move
            	Node child = new Node(childMove, side.opposite(), newBoard);
                child.setAlpha(alpha);
                child.setBeta(beta);
                addChild(child);
                newBoard = board.copy();
            }
        }

        public OthelloBoard getBoard() {
            return board;
        }

		public Move getMove() {
			return move;
		}

		public OthelloSide getSide() {
			return side;
		}

		public Node getParent() {
			return parent;
		}

		public LinkedList<Node> getChildren() {
			return children;
		}

        public void setChildren(LinkedList<Node> children) {
            this.children = children;
        }

        public double getScore() {
            return score;
        }

        public double getAlpha() {
            return alpha;
        }

        public double getBeta() {
            return beta;
        }

        public void setAlpha(double alpha) {
            this.alpha = alpha;
        }

        public void setBeta(double beta) {
            this.beta = beta;
        }

	}
}