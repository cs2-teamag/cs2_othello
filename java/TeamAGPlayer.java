// You will want to rename this to contain your team username.
import java.util.Vector;

public class TeamAGPlayer implements OthelloPlayer {

	private OthelloSide side;

	private OthelloSide otherSide;

	private OthelloBoard board;

	@Override
	public void init(OthelloSide side) {
		this.side = side;
		otherSide = this.side.opposite();
		board = new OthelloBoard();
	}

	@Override
	public Move doMove(Move opponentsMove, long millisLeft) {
		board.move(opponentsMove, otherSide);
		Vector<Move> moves = board.getMoves(side);
		if (moves.size() == 0)
			return null;
		DecisionTree tree = new DecisionTree(board, otherSide);
		Move moveToMake = tree.findBestMove(4);
		board.move(moveToMake, side);
		return moveToMake;
	}

	// You may define as many other methods as you feel you need.
}
