Week 1:

We discussed our strategies of weighting pieces and calculating board score 
together, and worked side-by-side to write our actual code. Since we were both 
in the same place and working on the same things at the same time, we decided 
it'd be easier if we typed all the modifications to our code on one person's 
computer because it'd be a little inconvenient to have to commit our files every 
time we made a small change. We didn't really split up the work and work on 
separate methods at different times or anything like that because we worked 
together on everything.

Week 2:

We talked about our overall goal and how we would implement the decision tree a 
bit together. Cindy considered different methods of generating and pruning the 
tree (because we were both finding alpha-beta pruning a little difficult to 
understand), and we discussed and decided to try our average-score approach 
(detailed later). Alexandra wrote the basics of the code, and then we debugged 
and got it working together. We tried to prune the tree but quickly realized that 
wouldn't work, so after trying to understand alpha-beta pruning a little better 
we implemented that instead by modifying our average-score tree a bit. Luckily 
it was easy to adapt since both use a depth-first search, and after the alpha-beta 
pruning was set up we ran it over and over and tweaked the values of our weightings 
until we were satisfied with it.

Ideas that didn't work:

In the beginning, we thought about doing a breadth first search on our decision 
tree. We wanted to go level by level so we could go as far down as time/memory 
would allow, but we had no idea how to implement it so we decided to try another 
approach. 

After we generated a decision tree for a root node, we looked at the calculated 
average scores for the root node's children and pruned the nodes that had an 
average in the lower 50% of all averages. Or at least......we tried to. The 
original tree worked fine, but the pruning didn't work out so well. When a new 
decision tree was generated to a depth 2 greater than the depth of the original 
decision tree, we ran into memory problems. We could not figure out what was 
wrong with it, so we decided to try a different approach.

Improvements:
Initially, we implemented a method that summed up different weights for various 
board positions:
- Corner weight: corners were worth the most because a corner piece cannot be 
captured
- Edge weight: edges were worth more than regular spaces because a edge piece 
can only be captured from moves in one direction
- Adjacent weight: positions that were adjacent to the corners is good if 
occupied by the opponent and bad if the corner is unoccupied because it allows 
the opponent to occupy a corner
- Diagonal weight: positions that are diagonal from the corners is good if occupied 
by opponent and bad if the corner is unoccupied (similar to adjacent weight)
- Moves weight: moves that lead to the most possible moves is worth more than other 
moves
- Frontier weight: we want to minimize the pieces we have that are adjacent to empty 
squares and maximize the opponent's pieces that are next to empty squares

We then implemented a decision tree with a depth of 4 that calculated the average
score for the children nodes that result from the root node to determine the best
move to make given a particular board state.

Finally, even though it was more difficult for us to understand initially, we 
decided to try alpha-beta pruning. Our search method recursively searches to depth 
4 and then chooses the move with the highest beta value, which guarantees us the 
highest minimum possible score. To calculate score, we did a simple subtraction of 
opponent pieces from our pieces, plus some weightings for special pieces like the 
corner pieces, edge pieces, frontier pieces, and the number of moves each player 
could make.